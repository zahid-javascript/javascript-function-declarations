// More Declarations
// Let's practice our knowledge of function declarations and conditional statements.

// Declare a function called maxOfTwo that takes in two numbers and returns the greater value.

function maxOfTwo(a,b){
    if (a >= b) {
        return a;
    } else {
        return b;
    }
}

console.log(maxOfTwo(5,10));
