// The Park Rangers in Death Valley National Park divide up the feed responsibilities 
// daily for the Bighorn Sheep population. Each sheep needs 2 lbs of ranger-provided food per day. 
// We need a function to alert how many lbs of food each Park Ranger shoud load that day, 
// based on the number of sheep and the number of rangers available.


// Let's start by building a function called feedPerRanger. 
// This function should accept two parameters: one representing the current population of sheep, 
// and one for the number of rangers available during the day.

// Inside the function, multiply the sheep population by 2 and divide it by the number of rangers. 
// Assign the result to a new variable called feedPerRanger.


// Finish coding the function by displaying an alert with the following format:
// Each ranger should load *number* lbs of feed today.
// Replace *number* with the value for the variable feedPerRanger.

function feedPerRanger(population, rangers) {
    let feedPerRanger = (population * 2) / rangers;
    console.log("Each ranger should load " + feedPerRanger + " lbs of feed today.");
}
feedPerRanger(1000,5);