// Problem Solving with Functions II
// Back at the Hoover Dam, the technicians have decided 
// they want more control of which generators are on and off. 
// They’ve asked us to develop a way to adjust the total megawatts 
// generated upon the switch-on or switch-off of a single, chosen generator.

// Define a new function named changePowerTotal that takes the following four parameters:
// The total power generated.
// The generator ID for the current generator.
// The generator status.
// The amount of power produced by the current generator.

// Inside the function, add an if statement checking if the status is equal to the string "on".

// If the status is "on", add the amount of power for that generator to the total power 
// generated and display an alert in the following format:
// Generator #2 is now on, adding 62 MW, for a total of 62 MW!

// Now let's add an else if statement that will run if the current generator status is "off".

// If the current generator's status is "off", subtract the amount of power for that generator 
// from the total power generated and display an alert in the following format:
// Generator #2 is now off, removing 62 MW, for a total of 0 MW!

// Finally, return the total power generated.

function changePowerTotal(total, ID, status, power) {
    if (status == "on") {
      total += power;
      console.log("Generator #" + ID + " is now " + status + ", adding " + power + " MW, for a total of " + total + " MW!");
    } else if (status == "off") {
      total -= power;
      console.log("Generator #" + ID + " is now " + status + ", removing " + power + " MW, for a total of " + total + " MW!");
    }
    return total;
}
changePowerTotal(1000,1,"on",200);