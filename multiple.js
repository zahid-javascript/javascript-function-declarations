// A Basic Multiplication Function
// Let's build a basic function that takes in three parameters and multiplies them.


// Start by declaring a function called multiplyTrio that takes in three parameters. 
// You can use whatever name you like for the parameters.

function multiplyTrio(a,b,c){
    return a*b*c;
}
console.log(multiplyTrio(10,1,3));